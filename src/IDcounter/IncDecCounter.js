import { React, useState } from 'react'
 
export default function App() {
  const [counter, setCounter] = useState(0);
 
    //Increase Counter
    const increase = () => {
        setCounter(count => count + 1);
    };
 
    //Decrease Counter
    const decrease = () => {
        if (counter > 0) {
            setCounter(count => count - 1);
        }
    };
 
    //Reset Counter 
    const reset = () =>{
        setCounter(0)
    }
 
  return (
    <div className="counter">
      <h1>React Counter</h1>
      <span className="counter__output">{counter}</span>
      <div className="btn__container">
        <button className="control__btn" onClick={increase}>Increment</button>
        <button className="control__btn" onClick={decrease}>Decrement</button>
        <button className="reset" onClick={reset}>Reset</button>
      </div>
    </div>
  );
}